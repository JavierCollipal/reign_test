import moment from 'moment';

export const formatPostDate = date => {
	const momentDate = moment(date).utc();
	const today = moment();
	const yesterday = moment().subtract(1, 'day');
	if (moment(momentDate).isSame(today, 'day')) return momentDate.format('HH:mm A');
	if (moment(momentDate).isSame(yesterday, 'day')) return 'Yesterday';
	else return `${momentDate.format('MMM')} ${momentDate.format('D')}`;
};
