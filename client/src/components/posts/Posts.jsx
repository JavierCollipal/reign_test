import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { deletePostAsync, fetchPostsAsync } from '../../reducers/posts';
import PostsTable from './PostsTable';

const Posts = ({ posts, fetchPosts, deletePost }) => {
	useEffect(() => fetchPosts(), [fetchPosts]);
	return (
		<div>
			<PostsTable posts={posts} handleDelete={deletePost} />
		</div>
	);
};

//store config
const mapStateToProps = state => ({ posts: state.posts.data });

const mapDispatchToProps = dispatch => ({
	fetchPosts: () => dispatch(fetchPostsAsync()),
	deletePost: id => dispatch(deletePostAsync({ id })),
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
