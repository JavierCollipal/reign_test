import React from 'react';
import './PostsTable.css';
import PropTypes from 'prop-types';

const tableStyle = {
	width: '100%',
};

const renderTable = (posts, handleDelete) => {
	return posts.map(post => {
		const { title, story_title, url, story_url, author, _id, created_at } = post;
		if (title || story_title)
			return (
				<tr key={_id} className="table_row">
					<td>
						<a
							href={url || story_url}
							target="_blank"
							rel="noreferrer"
							className="font-color"
						>
							{title || story_title}
						</a>
					</td>
					<td className="author-font-color">-{author}-</td>
					<td className="font-color">{created_at}</td>
					<td>
						<button onClick={() => handleDelete(_id)}>remover post</button>
					</td>
				</tr>
			);
		else return null;
	});
};

const PostsTable = ({ posts, handleDelete }) => {
	return (
		<div>
			<h1>Hacker news</h1>
			<table style={tableStyle}>
				<tbody>{renderTable(posts, handleDelete)}</tbody>
			</table>
		</div>
	);
};

PostsTable.propType = {
	posts: PropTypes.array.isRequired,
	handleDelete: PropTypes.func.isRequired,
};

export default PostsTable;
