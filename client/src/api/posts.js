import axios from 'axios';
const apiUrl = 'http://localhost:3000/post';

const fetchPosts = async () => await axios.get(apiUrl);
const deletePost = async id => await axios.delete(`${apiUrl}/${id}`);

const postApi = { fetchPosts, deletePost };

export default postApi;
