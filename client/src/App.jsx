import React from 'react';
import Posts from './components/posts/Posts';

function App() {
	return (
		<div>
			<Posts />
		</div>
	);
}

export default App;
