import postApi from '../api/posts';
import { formatPostDate } from '../utils/moment/functions';
//actions
export const FETCH = 'POST/FETCH';
export const DELETE_ONE = 'POST/DELETE_ONE';
//creators
export const fetchPosts = posts => ({
	type: FETCH,
	posts,
});

export const deleteOne = id => ({
	type: DELETE_ONE,
	id,
});
//state definition
export const initialState = {
	data: [],
};
//thunks
export const fetchPostsAsync = () => {
	return async dispatch => {
		try {
			const response = await postApi.fetchPosts();
			//formatting of dates is required here
			const formattedData = response.data.map(data => ({
				...data,
				created_at: formatPostDate(data.created_at),
			}));
			dispatch(fetchPosts(formattedData));
		} catch (e) {
			console.error(e);
		}
	};
};
export const deletePostAsync = ({ id }) => {
	return async dispatch => {
		try {
			await postApi.deletePost(id);
			dispatch(deleteOne(id));
		} catch (e) {
			console.error(e);
		}
	};
};
const postReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH:
			return { ...state, data: action.posts };
		case DELETE_ONE:
			return { ...state, data: state.data.filter(post => post._id !== action.id) };
		default:
			return { ...state };
	}
};

export default postReducer;
