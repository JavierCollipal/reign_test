import { Controller, Get, Delete, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getAll(): string {
    return this.appService.getHello();
  }
  @Delete(':id')
  async deleteOne(@Param('id') id): Promise<void> {
    await this.appService.getHello();
  }
}
