import { NestFactory } from '@nestjs/core';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import { AppModule } from './app.module';
const rabbitOptions = {
  urls: ['amqps://gorvmgvi:XhbqwGpqTowA79QuiJOxleHdil5DyVuu@moose.rmq.cloudamqp.com/gorvmgvi'],
  queue: 'posts_queue',
  queueOptions: {
    durable: true
  }
}
async function bootstrap() {

  const api = await NestFactory.create(AppModule);

  const microservice = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, { transport: Transport.RMQ , options: rabbitOptions});

  await api.listen(3000);
  await microservice.listen();
}
bootstrap();
