import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostsModule } from './posts/posts.module';
import { HealthModule } from './health/health.module';

@Module({
  imports: [PostsModule, HealthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
