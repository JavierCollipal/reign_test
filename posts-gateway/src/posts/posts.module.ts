import { Module } from '@nestjs/common';
import { PostsService } from './posts.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { PostsController } from './posts.controller';
import {rabbitOptions} from "./posts.utils";

@Module({
  imports:  [
    ClientsModule.register([
        {
            name: 'POSTS_SERVICE',
            transport: Transport.RMQ,
            options:  rabbitOptions
        }
  ])],
  controllers: [PostsController],
  providers: [PostsService]
})
export class PostsModule {}
