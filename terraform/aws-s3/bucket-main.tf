resource "aws_s3_bucket" "first"{

  bucket = "posts-s3-bucket"

}

resource "aws_s3_bucket_acl" "example1" {

  bucket = aws_s3_bucket.first.id

  acl    = "private"

}