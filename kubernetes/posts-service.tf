resource "kubernetes_deployment" "posts-service" {
  metadata {
    name = "posts-service"
    labels = {
      app  = "posts-collector-app"
      name = "posts-service"
    }
  }

  spec {
    type = "LoadBalancer"
    ports {
      port        = 3001
      target_port = 3
    }
    selector {
      match_labels = {
        app  = "posts-collector-app"
        name = "posts-service"
      }
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app  = "posts-collector-app"
        name = "posts-service-pod"
      }
    }
    template {
      metadata {
        labels = {
          app  = "posts-collector-app"
          name = "posts-service-pod"
        }
      }
      spec {
        container {
          name    = "posts-service"
          image   = "jcollipal/reign_server"
          command = ["npm", "run", "start:prod"]
          ports = {
            container_port = 3001
          }

          resources {
            limits {
              cpu    = "100m"
              memory = "128Mi"
            }
            requests {
              cpu    = "100m"
              memory = "128Mi"
            }
          }
        }
      }
    }
  }
}