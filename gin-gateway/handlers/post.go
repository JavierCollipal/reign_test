package postHandler

import (
	"fmt"
	"time"

	"context"

	"github.com/gin-gonic/gin"
	mongodb "gitlab.com/JavierCollipal/reign_test/gin-gateway/config"
	"gitlab.com/JavierCollipal/reign_test/gin-gateway/models"
	"gitlab.com/JavierCollipal/reign_test/gin-gateway/responses"
	"gitlab.com/JavierCollipal/reign_test/gin-gateway/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

var postCollection *mongo.Collection = mongodb.GetCollection(mongodb.Client, "posts")

func GetAll(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var posts []models.Post
	//TODO: study defer and why its cancelling the context
	defer cancel()
	query := bson.M{}
	results, err := postCollection.Find(ctx, query)
	fmt.Println(postCollection)
	fmt.Println(results)
	utils.ErrorHandler(err)
	defer results.Close(ctx)

	for results.Next(ctx) {
		var singlePost models.Post
		if err = results.Decode(&singlePost); err != nil {
			fmt.Println(err, "TODO: make an util for gin error handling")
		}
		posts = append(posts, singlePost)
	}
	c.JSON(200, responses.DataResponse{Message: "success", Data: map[string]interface{}{"Data": posts}})
}

func DeleteOne(ctx *gin.Context) {

	argument := ctx.Params[0]
	fmt.Println(argument)
	//work in progress
	ctx.JSON(200, argument)
}
