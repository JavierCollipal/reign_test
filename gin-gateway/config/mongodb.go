package mongodb

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/joho/godotenv"
	"gitlab.com/JavierCollipal/reign_test/gin-gateway/utils"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func envInit() {
	err := godotenv.Load(".env")
	utils.ErrorHandler(err)
}

// client become pointer?
func ConnectDb() *mongo.Client {

	envInit()

	MongoDb := os.Getenv("MONGODB_URL")
	//connection
	client, err := mongo.NewClient(options.Client().ApplyURI(MongoDb))
	utils.ErrorHandler(err)
	//ctx declaration
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	//TODO: study this
	defer cancel()

	err = client.Connect(ctx)
	utils.ErrorHandler(err)

	err = client.Ping(ctx, nil)
	utils.ErrorHandler(err)

	fmt.Println("Connected to MongoDB!")

	return client
}

// Client Database instance
var Client *mongo.Client = ConnectDb()

// with client pointer and target collection name, return a collection
func GetCollection(client *mongo.Client, collectionName string) *mongo.Collection {
	return client.Database("gin-gateway").Collection(collectionName)
}
