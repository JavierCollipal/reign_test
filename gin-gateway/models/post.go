package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Post struct {
	Title       string             `json:"title" validate:"required"`
	Created_at  string             `json:"created_at" validate:"required"`
	URL         string             `json:"url" validate:"required"`
	Author      string             `json:"author" validate:"required"`
	Story_id    int                `json:"story_id" validate:"required"`
	Story_title string             `json:"story_title" validate:"required"`
	Story_url   string             `json:"story_url validate:"required"`
	_id         primitive.ObjectID `json:"id,omitempty"`
}
