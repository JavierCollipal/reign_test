package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/JavierCollipal/reign_test/gin-gateway/handlers"	
)

func Posts(r *gin.Engine) *gin.Engine {
	r.GET("/posts", postHandler.GetAll)
	r.DELETE("/posts/:id", postHandler.DeleteOne)
	return r
}

func Configure(r *gin.Engine) *gin.Engine {
	Posts(r)
	return r
}
