package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/JavierCollipal/reign_test/gin-gateway/router"
	"gitlab.com/JavierCollipal/reign_test/gin-gateway/config"
)

func main() {
	r := gin.Default()

	mongodb.ConnectDb()

	router.Configure(r)

	r.Run()
}
