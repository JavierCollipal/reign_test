import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PostDocument = Post & Document;

@Schema()
export class Post {
  @Prop()
  title: string;
  @Prop()
  created_at: Date;
  @Prop()
  url: string;
  @Prop()
  author: string;
  @Prop()
  story_id: number;
  @Prop()
  story_title: string;
  @Prop()
  story_url: string;
  @Prop()
  objectID: string;
}

export const PostSchema = SchemaFactory.createForClass(Post);
