import {  Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios'
import { TaskService } from './task.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Post, PostSchema } from '../schemas/post.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }]),
    HttpModule,
  ],
  providers: [TaskService],
})
export class TaskModule {}
