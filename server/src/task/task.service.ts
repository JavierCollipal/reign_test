import {  Injectable,OnModuleInit, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios'
import { HACKER_NEW_URL } from '../constants';
import { Model } from 'mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectModel } from '@nestjs/mongoose';
import { Post, PostDocument } from '../schemas/post.schema';

@Injectable()
export class TaskService implements OnModuleInit {
  constructor(
    @InjectModel(Post.name)
    private postModel: Model<PostDocument>,
    private httpService: HttpService,
  ) {}

  async onModuleInit(): Promise<void> {
    await this.fetchHackerNews();
  }

  @Cron(CronExpression.EVERY_HOUR)
  async fetchHackerNews(): Promise<void> {
    try {
      const { data } = await this.httpService.get(HACKER_NEW_URL).toPromise();
      await this.postModel.insertMany(data.hits);
    } catch (e) {
      Logger.error(e.message);
    }
  }
}
