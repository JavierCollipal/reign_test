import { Test } from '@nestjs/testing';
import { TaskService } from './task.service';
import { HttpModule, HttpService } from '@nestjs/axios'
import { HACKER_NEW_URL } from '../constants';
import { mockPosts } from '../utils/mocks/posts/data';
import { getModelToken } from '@nestjs/mongoose';
import { Post } from '../schemas/post.schema';
import { of } from 'rxjs';
import { AxiosResponse } from 'axios';
import { postModel } from '../utils/mocks/posts/model';

const mockResult: AxiosResponse = {
  data: { hits: mockPosts },
  status: 200,
  statusText: 'Ok',
  headers: {},
  config: {},
};

describe('TaskService', () => {
  let service: TaskService;
  let httpService: HttpService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        TaskService,
        { provide: getModelToken(Post.name), useValue: postModel },
      ],
    }).compile();
    service = moduleRef.get<TaskService>(TaskService);
    httpService = moduleRef.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('fetchHackerNews()', () => {
    //an axiosResponse object and of(rxjs) are needed to spy the httpService call
    it('should call httpService->get with hackerNew url', async () => {
      const mockGet = jest
        .spyOn(httpService, 'get')
        .mockImplementationOnce(() => of(mockResult));

      await service.fetchHackerNews();
      expect(mockGet).toBeCalledWith(HACKER_NEW_URL);
    });
    //this case is commented, because trying to test Observable value was consuming time with no results.
    /*it('httpModule->get() should retrieve an array of posts', async () => {

      const mockGet = jest.spyOn(httpService, 'get').mockImplementationOnce(() => mockObservable).mockReturnValue(mockResult);

      await service.fetchHackerNews()
      expect(mockGet.mock.results[0].value).toBe(mockPosts)
    });*/
    it('finally fetchHackerNews(), should call postModel->insertMany() with the found posts as params', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockImplementationOnce(() => of(mockResult));

      await service.fetchHackerNews();
      expect(postModel.insertMany).toBeCalledWith(mockPosts);
    });
  });

});
