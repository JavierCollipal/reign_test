import { Test, TestingModule } from '@nestjs/testing';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { getModelToken } from '@nestjs/mongoose';
import { Post } from '../schemas/post.schema';
import { postModel } from '../utils/mocks/posts/model';
import { mockPosts, mockPostUUID } from '../utils/mocks/posts/data';
import { BadRequestException } from '@nestjs/common';

describe('PostService', () => {
  let service: PostService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostController],
      providers: [
        PostService,
        { provide: getModelToken(Post.name), useValue: postModel },
      ],
    }).compile();
    service = module.get<PostService>(PostService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAll()', () => {
    it('should call postModel->find()', async () => {
      const mockFind = jest.spyOn(postModel, 'find');
      await service.getAll();
      expect(mockFind).toHaveBeenCalled();
    });
    it('should return the results from postModel->find()', async () => {
      const foundPosts = await service.getAll();
      expect(foundPosts).toEqual(mockPosts);
    });
    it('should catch error on the code block', async () => {
      try {
        await service.getAll().then(() => {throw new BadRequestException('soy un error malisimo')})
      } catch (e) {
        expect(e).toBeInstanceOf(BadRequestException)
      }
    });
  });

  describe('deleteOne()', () => {
    it('should call postModel->deleteOne() with the post id in filter query', async () => {
      const mockQuery = { _id: mockPostUUID };
      const mockUpdateOne = jest.spyOn(postModel, 'deleteOne');
      await service.deleteOne(mockPostUUID);
      expect(mockUpdateOne).toBeCalledWith(mockQuery);
    });
  });
});
