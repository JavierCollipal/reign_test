import { Test, TestingModule } from '@nestjs/testing';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { mockPosts, mockPostUUID } from '../utils/mocks/posts/data';
import { getModelToken } from '@nestjs/mongoose';
import { Post } from '../schemas/post.schema';
import { postModel } from '../utils/mocks/posts/model';

describe('PostController', () => {
  let controller: PostController;
  let service: PostService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostController],
      providers: [
        PostService,
        { provide: getModelToken(Post.name), useValue: postModel },
      ],
    }).compile();

    controller = module.get<PostController>(PostController);
    service = module.get<PostService>(PostService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAll()', () => {
    it('should return an array of posts', async () => {
      jest.spyOn(service, 'getAll').mockImplementation(() => mockPosts);
      expect(await controller.findAll()).toBe(mockPosts);
    });
  });
  describe('deleteOne()', () => {
    it('should call postService->deleteOne() with the post id as param', async () => {
      const mockDeleteOne = jest
        .spyOn(service, 'deleteOne')
        .mockImplementation();
      await controller.deleteOne(mockPostUUID);
      expect(mockDeleteOne).toHaveBeenCalledWith(mockPostUUID);
    });
  });
});
