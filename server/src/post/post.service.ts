import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Post, PostDocument } from '../schemas/post.schema';
import {MessagePattern} from "@nestjs/microservices";

@Injectable()
export class PostService {
  constructor(
    @InjectModel(Post.name)
    private postModel: Model<PostDocument>,
  ) {}

  async getAll(): Promise<Post[]> {
    try {
      return this.postModel
        .find({})
        .sort({ created_at: 'desc' })
        .exec();
    } catch (e) {
      throw new BadRequestException('no fue posible obtener los posts')
    }
  }
  @MessagePattern({'role': 'post', 'cmd': 'deleteOne'})
  async deleteOne(id): Promise<void> {
    try {
      const $match =  { _id: id }
      await this.postModel.deleteOne($match);
    } catch (e) {
      throw new BadRequestException('no fue posible eliminar el post')
    }
  }
}
