import { Controller, Delete, Get, Inject, Param } from '@nestjs/common';
import { MessagePattern } from "@nestjs/microservices";
import { PostService } from './post.service';
import { Post } from '../schemas/post.schema';

@Controller()
export class PostController {
  constructor(@Inject(PostService) private postService: PostService) {}

  @Get()
  @MessagePattern({ cmd: 'findAll'})
  async findAll(): Promise<Post[]> {
    return await this.postService.getAll();
  }

  @Delete(':id')
  @MessagePattern({ cmd: 'deleteOne' })
  async deleteOne(id: string): Promise<void> {
    await this.postService.deleteOne(id);
  }
}
