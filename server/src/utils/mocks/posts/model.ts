import { mockPosts } from './data';
export const postModel = {
  insertMany: jest.fn(),
  find: () => ({
    sort: jest.fn().mockReturnValue({ exec: async () => mockPosts }),
  }),
  deleteOne: jest.fn(),
  updateOne: jest.fn(),
};
