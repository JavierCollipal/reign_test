import { NestFactory } from '@nestjs/core';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const rabbitOptions = {
    urls: ['amqps://gorvmgvi:XhbqwGpqTowA79QuiJOxleHdil5DyVuu@moose.rmq.cloudamqp.com/gorvmgvi'],
    queue: 'posts_queue',
    queueOptions: {
      durable: true
    }
  }
  const api = await NestFactory.create(AppModule);

  const micro = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, { transport: Transport.RMQ , options: rabbitOptions});
  await api.listen(3001);

  await micro.listen();
}
bootstrap();
