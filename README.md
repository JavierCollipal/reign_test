# Posts_microservice 
# Instalación docker
 1- docker-compose build <br>
 2- docker-compose up <br>
 3- visitar localhost:80 o localhost

# Instalación local
 1- cd client, npm install, npm run start <br>
 2- cd server, npm install, npm run start <br>
 3- visitar localhost:80 o localhost


# Instalación kuberntenes 
 1- cd kubernetes, 
 2- kubectl apply -f 'archivo.yaml' a cada uno de los archivos
 3- visitar localhost:80 o localhost
# Testing
1- cd server<br>
2- npm install<br>
3- npm run test:cov<br>